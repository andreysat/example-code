<?php
require_once "LocalSettings.php";
requireLogin();

Navigation::userNavi();

$packet_group_cont = new PacketGroupContainer();
$user_pac_gr_cont = new UserPacketGroupContainer();
$user_pac_gr_cont->loadActive();
$user_packet_groups = $user_pac_gr_cont->getAll();


foreach ($user_packet_groups as $login => $packet_groups_tmp) foreach ($packet_groups_tmp as $packet_group => $user_packet_group_all) 
    foreach ($user_packet_group_all as $packet_group_key => $user_packet_group){
        if ($user_packet_group->get('time_from')>time()) continue;
        //$login = $user_packet_group->get('login');
        $packet_gr[$login][] = $user_packet_group->get('packet_group')." до ". date("d-m-Y H:i:s", $user_packet_group->get('time_till'));
}
$tpl->assign("packet_gr", $packet_gr);


$dealed_con = new HandledUserContainer("", $_GET['order_by'], $_GET['order_asc']);
if(!isset($_GET['filter_login'])){
    $ph = $dealed_con->loadByDealer($_SESSION['user_login']);
} else{
    $ph = $dealed_con->loadByDealerWithFilter($_GET['filter_login'],$_SESSION['user_login']);
}
$tpl->assign("items_pp", $dealed_con->getItemsPP());
$tpl->assign('dealed_users', $dealed_con->getAll());

if (isset($_POST['save'])){
    $dealed_result = 1;
    $user_tc = new User($_POST['login']);
    if (!$user_tc->isLoaded())
        $user_tc->set('time_added', time());
    else
    {
        //print_r($user_tc);
        //print "User login = " . $_SESSION['user_login'];
        //print "Dealer = " . $user_tc->get("dealer");
        // Check if we try to overwrite an existing user!!!
        if ($user_tc->get("dealer") != $_SESSION['user_login'] || $user_tc->get("dealer") == "")
        {
            $dealed_result = 0;
            Messages::setUserErrorMsg($lang->get("err__cannot_overwrite_user"));
            //print "What the FUCK!";
            //print "Result is " . $dealed_result;
        }
    }
    $user_tc->setData($_POST);
    $user_tc->set('time_edited', time());
    $user_tc->set('disabled', intval($_POST["disabled"]));
    $user_tc->set('confirmed', 1);
    $user_tc->set('dealer', $_SESSION['user_login']);
    $server_n = intval($_POST["server_n"]);
    if ($server_n <= 0 || $server_n > ConstTrans::TRANS_SERVER_COUNT)
    {
        $sl = new ServerLoad();
        $server_n = $sl->getLeastLoadedServer();
    }
    $user_tc->set('server_n', $server_n);
    if (!$user_tc->validate())
        $dealed_result = 0;
    //print "Result is " . $dealed_result;
    if ($_POST['password_new'] != "")
        $dealed_result = $dealed_result && ($user_tc->setPassword($_POST['password_new'], $_POST['password_confirm']));
    //print "Result is " . $dealed_result;
    if ($dealed_result)
    {
        if (!$user_tc->isLoaded()){
            BalanceChange::saveBalanceHistory(array(
                'amount' =>  (new User($_SESSION['user_login']))->get('balance'),
                'reason' => "Добавление нового логина",
                'residue' => "",
                'details' => "",
                'reason_login' => $_POST['login'],
            ));
        }
        $user_tc->save();
        $ssn->redirectTo($_SERVER['PHP_SELF']);
        die();
    }
    $tpl->assign('user', $user_tc);
} elseif (isset($_GET['edit'])){
    $tpl->assign('user', $dealed_con->get($_GET['edit']));
} elseif (isset($_GET['delete'])){
    $user_pac_gr_cont->stopAllPacket($_GET['delete']);
    $to_delete = $dealed_con->get($_GET['delete']);
    if (is_object($to_delete)) {
        $to_delete->delete();
        $ssn->redirectTo($_SERVER['PHP_SELF']);
    }
}

$tpl->assign("table_fields", array(
    'login'         => $lang->get("label__user_login"),
    'password'      => $lang->get("label__user_password"),
    'notation'      => $lang->get("label__notation"),
    'server_n'      => $lang->get("label__user_server_n"),
    'time_added'    => $lang->get("label__user_time_added"),
    'time_edited'   => $lang->get("label__user_time_edited"),
));

$tpl->assign("field_modifiers", array(
    'time_added'    => "date",
    'time_edited'   => "date"
));

Messages::formatAll();
$tpl->assign("page_title", $lang->get("navi__dealer"));
$tpl->assign("main_template", "User_Dealer.html");
$tpl->display('Fend_Index.html');
?>