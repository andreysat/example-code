<?php
require_once "LocalSettings.php";
requireLogin();
Navigation::userNavi();

$user = $GLOBALS['user'];
$tpl->assign('user', $user);
if (isset($_POST['save'])) {
    // защита от хаЦкеров
    unset($_POST['balance']);
    unset($_POST['discount']);
    unset($_POST['confirmed']);
    unset($_POST['disabled']);
    unset($_POST['login']);
    unset($_POST['password']);

    $user->setData($_POST);
    if ($user->validate()) {
        $user->save();
        $ssn->redirectTo($_SERVER['PHP_SELF']);
    }
}

Messages::formatAll();
$tpl->assign("main_template", "User_Profile.html");
$tpl->display('Fend_Index.html');
?>
