<?php

require_once "LocalSettings.php";
requireLogin();

Navigation::userNavi();

// Список тех, чьим дилером является данный пользователь
$dealed_cont = new UserContainer();
$dealed_cont->loadByDealerWithDealer($_SESSION['user_login']);
$user_logins = array_keys($dealed_cont->getAll());
if ($user_logins != array() && !empty($user_logins))
    $tpl->assign("user_logins", $user_logins);

// Кому каналы покупаем?
$selected_user = $_REQUEST['selected_user'];

if ($selected_user == "" || !in_array($selected_user, $user_logins))
    $selected_user = $_SESSION['user_login'];
$tpl->assign("selected_user", $selected_user);


// Группы пакетов, активные для данного пользователя
$user_p_g_cont = new UserPacketGroupContainer("", $_GET['order_by'], $_GET['order_asc']);
$user_p_g_cont->loadActiveByUser($selected_user);
$user_p_g = $user_p_g_cont->getAll();
#print_r($user_p_g);
$user_p_g = $user_p_g[$selected_user];

$packet_group_cont = new PacketGroupContainer();
$packet_group_cont->loadAll();
$tpl->assign('packet_groups', $packet_group_cont->getAll());


if (isset($_POST['submit']))
{
    // забекапим старый список групп каналов пользователя
    $user_p_g_bak = $user_p_g;
    $login = $selected_user;
    $day_count = intval($_POST['day_count']);
    if ($day_count < 0)
	 Messages::setUserErrorMsg("Дата завершения подписки не может быть меньше даты начала просмотра!!!");
      if (Messages::userErrorsEx() == 0)
    {
    $total_price = 0;
    $data1e = explode('-', $_POST['data1']);
    $cur_time = max(time(), mktime(0, 0, 0, $data1e[1], $data1e[0], $data1e[2]));
    $data1e = explode('-', $_POST['data2']);
    $cur_time2 = $cur_time + 86400 * $day_count;
	
	if(!empty($_POST['user_packet_groups']))
	 {
    foreach ($_POST['user_packet_groups'] as $packet_group)
    {
        if (empty($user_p_g[$packet_group]))
            $user_p_g[$packet_group] = array();

        $cur_user_packet_group = new UserPacketGroup();
        $cur_user_packet_group->set('login', $login);
        $cur_user_packet_group->set('packet_group', $packet_group);
        $cur_user_packet_group->set('time_from', $cur_time);
        $cur_user_packet_group->set('time_till', $cur_time2);

        $stampa = $cur_user_packet_group->get('time_from');
        $stampa1 = $cur_user_packet_group->get('time_till');
        foreach ($user_p_g[$packet_group] as $cur_user_packet_group2)
        {
            $stampa2 = $cur_user_packet_group2->get('time_from');
            $stampa3 = $cur_user_packet_group2->get('time_till');
            if ($stampa > $stampa2 and $stampa < $stampa3)
            {
                $stampa4 = $stampa3 - $stampa;
                $stampa += $stampa4;
                $stampa1 += $stampa4;
                $cur_user_packet_group->set('time_from', $stampa);
                $cur_user_packet_group->set('time_till', $stampa1);
                continue;
            }
        }
        $user_p_g[$packet_group][] = $cur_user_packet_group;

        $cur_packet_group = $packet_group_cont->get($packet_group);
        $total_price += $day_count * ($cur_packet_group->get('price'));
    }
	}
    // Хватит ли у пользователя денег?
    //$user = $GLOBALS['user'];
    $user_balance = $user->get('balance');
    $user_discount = $user->get('discount');
    $total_price *= ((100 - $user_discount) / 100);
    if ($total_price <= $user_balance AND $cur_user_packet_group = $user_p_g[$packet_group] )
    {
        // Всё ОК
        $details = "-".$day_count." дней подписки";
        $reason = "Куплен пакет: ".$packet_group; 
        $user->set('balance', $user_balance - $total_price);
         $bal_change = new BalanceChange();
        $bal_change->set('login',   $_SESSION['user_login']);
        $bal_change->set('time',    time());
        $bal_change->set('amount',  -$total_price);
        $bal_change->set('reason',   $reason);
        $bal_change->set('residue',   $user->get("balance") );
        $bal_change->set('details',    $details);
        $bal_change->set('reason_login',    $selected_user);
        $bal_change->set('ip',    $_SERVER['REMOTE_ADDR']);
        $bal_change->save();                
        $user->save();
        foreach ($_POST['user_packet_groups'] as $packet_group)
        foreach ($user_p_g[$packet_group] as $cur_user_packet_group)        
        {
            $cur_user_packet_group->save();
        }
        // -------------
        DaemonControl::regenConfig();
        DaemonControl::control("rehash");
        $ssn->redirectTo($_SERVER['PHP_SELF'] . "?selected_user=" . $selected_user);
    } else
    {
        Messages::setUserErrorMsg($lang->get("err__not_enough_balance", array('total_price' =>
            $total_price, 'balance' => $user_balance)));
        $user_p_g = $user_p_g_bak;
    }
}
}
if (count($user_p_g)) foreach ($user_p_g as $packey=>$packet_group) {
    $cur_user_packet_group = array_shift($packet_group);
    $cur_user_packet_group->get('time_till');
    while ($cur_user_packet_group2=array_shift($packet_group)) {
        if ($cur_user_packet_group->get('time_till')<$cur_user_packet_group2->get('time_till')) 
        $cur_user_packet_group = $cur_user_packet_group2;
    }
    $user_p_g[$packey] = $cur_user_packet_group;
}
$tpl->assign("user_packet_groups", $user_p_g);

$tpl->assign("table_fields", array('packet_group' => $lang->get("label__packet_group"),
    'time_till' => $lang->get("label__time_till")));

$tpl->assign("date1show", date("d-m-Y"));
$tpl->assign("date2show", date("d-m-Y", mktime(date("H"), date("i"), date("s"),
    date("n") + 1, date("j"), date("Y"))));

$tpl->assign('page_title', $lang->get('title__packets'));
Messages::formatAll();
$tpl->assign("main_template", "User_Packets.html");
$tpl->display('Fend_Index.html');

?>