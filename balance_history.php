<?php
require_once "LocalSettings.php";
requireLogin();

Navigation::userNavi();

$bal_con = new HandledBalanceChangeContainer("", $_GET['order_by'], $_GET['order_asc']);
$ph = $bal_con->loadByUser($_SESSION['user_login']);
$tpl->assign("items_pp", $bal_con->getItemsPP());
$tpl->assign('balance_history', $bal_con->getAll());

$tpl->assign("table_fields", array(
    'time'          => $lang->get("label__time"),
    'amount'        => $lang->get("label__amount"),
    'residue'        => $lang->get("label__residue"),
    'reason'        => $lang->get("label__reason"),
    'details'        => $lang->get("label__details"),
    'reason_login'        => $lang->get("label__login"),
    'ip'        => "ip",
   )); 

$tpl->assign("field_modifiers", array(
    'time'          => "time"
));

Messages::formatAll();
$tpl->assign("page_title", $lang->get("navi__balance_history"));
$tpl->assign("main_template", "User_Balance_History.html");
$tpl->display('Fend_Index.html');
?>

